// Implements a dictionary's functionality

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include<string.h>
#include<ctype.h>
#include "dictionary.h"

// Represents number of children for each node in a trie
#define N 27

// Represents a node in a trie
typedef struct node
{
    bool is_word;
    struct node *children[N];
}
node;

// Represents a trie
node *root;
unsigned int count;
// Loads dictionary into memory, returning true if successful else false
bool load(const char *dictionary)
{
    // Initialize trie
    root = malloc(sizeof(node));
    if (root == NULL)
    {
        return false;
    }
    root->is_word = false;
    for (int i = 0; i < N; i++)
    {
        root->children[i] = NULL;
    }

    // Open dictionary
    FILE *file = fopen(dictionary, "r");
    if (file == NULL)
    {
        unload();
        return false;
    }

    // Buffer for a word
    char word[LENGTH + 1];

    // Insert words into trie
    while (fscanf(file, "%s", word) != EOF)
    {
        int len = strlen(word);
        int prev = -1, i, x, j;
        char ch;
        node* p = root;
        node *tmp;
        for(i = 0; i < len; i++){
            ch = word[i];
            if(ch == '\'')
                x  = 26;
            else{
                if(!isupper(ch))
                    x = ch - 'a';
                else
                    x = ch - 'A';
            }
            if(prev != -1){
                if(p->children[prev] == NULL){
                    tmp = (node*)malloc(sizeof(node));
                    tmp->is_word = 0;
                    for(j = 0; j < 27; j++){
                        tmp->children[j] = NULL;
                    }
                    p->children[prev] = tmp;
                    p = p->children[prev];
                }
                else{
                    p = p->children[prev];
                }
            }
            prev = x;
        }
        if(p->children[prev] == NULL){
            tmp = (node*)malloc(sizeof(node));
            tmp->is_word = 0;
            for(j = 0; j < 27; j++)
                tmp->children[j] = NULL;
            tmp->is_word = 1;
            count++;
            p->children[prev] = tmp;
        }
        else{
            p->children[prev]->is_word = 1;
            count++;
        }
        
    }

    // Close dictionary
    fclose(file);

    // Indicate success
    return true;
}

void rsize(node *r){
   if(r == NULL)
        return;
    for(int i = 0; i < 27; i++){
    	if(r->children[i] != NULL)
        	rsize(r->children[i]);	
    }
    /*if(r->children[0] != NULL)
        rsize(r->children[0]);
    if(r->children[1] != NULL)
        rsize(r->children[1]);
    if(r->children[2] != NULL)
        rsize(r->children[2]);
    if(r->children[3] != NULL)
        rsize(r->children[3]);
    if(r->children[4] != NULL)
        rsize(r->children[4]);
    if(r->children[5] != NULL)
        rsize(r->children[5]);
    if(r->children[6] != NULL)
        rsize(r->children[6]);
    if(r->children[7] != NULL)
        rsize(r->children[7]);
    if(r->children[8] != NULL)
        rsize(r->children[8]);
    if(r->children[9] != NULL)
        rsize(r->children[9]);
    if(r->children[10] != NULL)
        rsize(r->children[10]);
    if(r->children[11] != NULL)
        rsize(r->children[11]);
    if(r->children[12] != NULL)
        rsize(r->children[12]);
    if(r->children[13] != NULL)
        rsize(r->children[13]);
    if(r->children[14] != NULL)
        rsize(r->children[14]);
    if(r->children[15] != NULL)
        rsize(r->children[15]);
    if(r->children[16] != NULL)
        rsize(r->children[16]);
    if(r->children[17] != NULL)
        rsize(r->children[17]);
    if(r->children[18] != NULL)
        rsize(r->children[18]);
    if(r->children[19] != NULL)
        rsize(r->children[19]);
    if(r->children[20] != NULL)
        rsize(r->children[20]);
    if(r->children[21] != NULL)
        rsize(r->children[21]);
    if(r->children[22] != NULL)
        rsize(r->children[22]);
    if(r->children[23] != NULL)
        rsize(r->children[23]);
    if(r->children[24] != NULL)
        rsize(r->children[24]);
    if(r->children[25] != NULL)
        rsize(r->children[25]);
    if(r->children[26] != NULL)
        rsize(r->children[26]);*/
    free(r);
}
// Returns number of words in dictionary if loaded else 0 if not yet loaded
unsigned int size(void)
{
    return count;
}

// Returns true if word is in dictionary else false
bool check(const char *word)
{
    int len = strlen(word);
    int i, x;
    node *p = root;
    char ch;
    
    for(i = 0; i < len; i++){
        ch = word[i];
        if(ch == '\''){
            x = 26;
        }
        else{
            if(isupper(ch))
                x = ch - 'A';
            else
            x = ch - 'a';
        }
        if(p != NULL){
            p = p->children[x];
        }
        else{
            return false;
        }
        
    }
    if(p != NULL){
        if(p->is_word == 1){
            return true;
        }
        else    
            return false;
        
    }
    return false;
}

// Unloads dictionary from memory, returning true if successful else false
bool unload(void)
{
    rsize(root);
    return true;
}
